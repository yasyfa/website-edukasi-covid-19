from django.test import TestCase, Client
from .models import *
from .views import *

class UnitTestScreeningTests(TestCase):
    def screening_test_is_exist(self):
        response = self.client.get('/uji-screening/test/')
        self.assertEqual(response.status_code, 200)
    
    def screening_res_is_exist(self):
        response = self.client.get('/uji-screening/result/')
        self.assertEqual(response.status_code, 200)

    def test_screening_test_using_correct_template(self):
        response = self.client.get('/uji-screening/test/')
        self.assertTemplateUsed(response, 'screening_tests/test.html', 'base.html')