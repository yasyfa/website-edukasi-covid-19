from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('test/', views.test, namespace='screening_test'),
    path('result/', views.result, namespace='screening_res'),
]