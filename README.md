# Website Edukasi COVID 19

[![pipeline status](https://gitlab.com//tk-1-ppw-2020/website-edukasi-covid-19/badges/master/pipeline.svg)](https://gitlab.com/tk-1-ppw-2020/website-edukasi-covid-19/-/commits/master)
[![coverage report](https://gitlab.com//tk-1-ppw-2020/website-edukasi-covid-19/badges/master/coverage.svg)](https://gitlab.com/tk-1-ppw-2020/website-edukasi-covid-19/-/commits/master)

Halo! Kami adalah kelompok D11 mata kuliah Perancangan dan Pemrograman Web kelas D Fasilkom UI. Pada TK1 ini, kami akan membuat sebuah website bernama EDUCOVID.

#### Nama anggota:
1. Najwa Lathifah Ulima - 1906398925
2. Muhammad Fayaad - 1906398433
3. Yasyfa Azkaa Wiwaha - 1906398465
4. Aisyah Indonesia Mazaya Zayn - 1906399000
5. Beltsazar Anugrah Sotardodo - 1906398351

#### Link herokuapp EDUCOVID:
https://educovid.herokuapp.com/

**EDUCOVID** merupakan website edukasi mengenai virus Covid-19 yang saat ini sedang mewabah di Indonesia dan hampir seluruh dunia. Pada website ini, user akan memperoleh  informasi umum seputar Covid-19, seperti definisi, gejala, cara penularan, dan cara pencegahan virus Covid-19. Selain itu, **EDUCOVID** juga memiliki halaman yang berisi daftar rumah sakit dari berbagai daerah yang menangani pasien Covid-19 beserta alamat dan nomor teleponnya. 

**EDUCOVID** juga memiliki beberapa fitur, di antaranya:
1. Search pada page RS rujukan. Fitur ini akan memudahkan user untuk mencari rumah sakit yang ingin mereka tuju.
2. Form uji screening, di mana user akan menjawab beberapa pertanyaan mengenai gejala Covid-19 yang mereka alami, lalu web akan menunjukkan persentase kemungkinan user tersebut terkena Covid-19. Fitur ini bukan untuk memberikan diagnosis yang akurat, namun hanya untuk meningkatkan awareness agar user menjaga kesehatannya.
3. Menampilkan jumlah kasus Covid-19 di Indonesia pada homepage yang diambil menggunakan API. Fitur ini ditampilkan agar user mengetahui jumlah kasus Covid-19 ter-update.
4. Form untuk user memberikan pendapatnya mengenai Covid-19 di page Edukasi. Fitur ini dibuat untuk mengetahui pandangan setiap user atas keberadaan Covid-19.
5. Form untuk user memberikan kesan, saran, kritik terhadap web kami di page About. Fitur ini dibuat untuk menjadi bahan evaluasi kami sebagai pengembang web.

Melalui **EDUCOVID**, kami harap kami dapat berkontribusi dalam memberikan edukasi dan informasi serta meningkatkan awareness para pengunjung website terhadap COVID-19. 
