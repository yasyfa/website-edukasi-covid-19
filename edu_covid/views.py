from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'edu_covid/index.html')

def edukasi(request):
    return render(request, 'edu_covid/edukasi.html')