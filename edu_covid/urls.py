from django.urls import path
from . import views

app_name = 'edu_covid'

urlpatterns = [
    path('', views.index, name='index'),
    path('edukasi/', views.edukasi, name='edukasi'),
]