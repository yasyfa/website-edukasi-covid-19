from django.apps import AppConfig


class EduCovidConfig(AppConfig):
    name = 'edu_covid'
